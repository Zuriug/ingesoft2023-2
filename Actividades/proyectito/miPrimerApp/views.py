from django.shortcuts import render, HttpResponse
from .models import *


# Create your views here.
def index(request):

    # varEstudiante = Estudiante.objects.get(nombres="Frida", apellidos="Landaverde")
    # print(varEstudiante)


    varEstudianteG1 = Estudiante.objects.filter(grupo=1)
    varEstudianteG2 = Estudiante.objects.filter(grupo=2)
    varEstudianteG3 = Estudiante.objects.filter(grupo=3)
    varEstudianteG4 = Estudiante.objects.filter(grupo=4) 
    varEstudianteG5 = Estudiante.objects.filter(grupo=5)
    varMismoAp = Estudiante.objects.filter(apellidos = 'Gómez')
    varMismaEdad = Estudiante.objects.filter(edad = '21')
    varEstudianteG3y22 = Estudiante.objects.filter(grupo=3, edad = 22)
   



    return render(request,'index.html', {'varEstudianteG1':varEstudianteG1,'varEstudianteG2':varEstudianteG2, 'varEstudianteG3':varEstudianteG3,
                                         'varEstudianteG4': varEstudianteG4,'varEstudianteG5': varEstudianteG5,'varMismoAp': varMismoAp, 
                                         'varMismaEdad': varMismaEdad,'varEstudianteG3y22' :varEstudianteG3y22})
